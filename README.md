# CTF usefull links



## A curated list of usefull CTF links

### Radare2
- [ ] [Radare2 Cheetsheet in PDF ](https://scoding.de/uploads/r2_cs.pdf) 
- [ ] [The Official Radare2 Book](https://book.rada.re/first_steps/intro.html)
- [ ] [R2 wiki](https://r2wiki.readthedocs.io/en/latest/home/misc/cheatsheet/)
- [ ] [Another one r2 cheetsheet](https://gist.github.com/williballenthin/6857590dab3e2a6559d7)
