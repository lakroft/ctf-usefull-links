radare2:
    r2 -d <binary_file_name>    - run debug of the programm
    dcu main                    - execute until main method
    dcu 0x00401383              - execute until address
    v                           - start visual mode
    s                           - step forward/inter the method
    S                           - step forward (withuot intering methods)
    px 10 @rdx-4                - gives 10 bytes starting from adress stored in rbx register minus 4 bytes
    ood                         - restart programm execution
    Ctrl+z                      - move radare to background and show shell
    fg                          - return radare back to foregrownd
    db 0x00401383               - Set breakpoint
    db -0x00401383              - Remove breakpoint
    db                          - List breakpoints
    dc                          - Continue execution
    r2 -r ./profile.rr2 -d ret2win         - debug with profile and stored input

How to bebug with profile:
    - create new terminal
        - make 'tty' command. You will get terminal dev adress like '/dev/pts/6'
        - make 'sleep inf'
    - create profile.rr2 file (what gonna happend if I use file as stdout - it's working)
        -  content of the file:
            #!/usr/bin/rarun2
            stdout=/dev/pts/6
            stdin=./input
        - In here '/dev/pts/6' is a terminal dev adress from the first step
    - create 'input' file
        - put into this file what you want to use as input in your debug process
    - run radare with command 'r2 -r ./profile.rr2 -d ret2win'
        - here is 'ret2win' is a binary file of the program you want to debug



use xxd for \x41\x41 <-> AA
use "echo -e 'AA\x41'"

Find func address in bin file:
    * `readelf -s libc.so.6 | grep puts`
    * `objdump -T /lib/x86_64-linux-gnu/libc.so.6 | grep puts`

Find gadgets in bin file:
    * one_gadget {filename}

Find useful gadgets in bin file:
    * ROPgadget -binary libc.so.6


Useful links
    - https://github.com/historypeats/radare2-cheatsheet
    - https://r2wiki.readthedocs.io/en/latest/home/misc/cheatsheet/
    - https://book.rada.re/first_steps/intro.html - The Official Radare2 Book
    - https://r2wiki.readthedocs.io/en/latest/options/d/db/ - db commands on Wiki
    - https://r2wiki.readthedocs.io/en/latest/options/d/dc/dcu/ - dcu commands on Wiki